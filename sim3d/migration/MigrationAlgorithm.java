package sim3d.migration;

import sim3d.cell.BC;

public interface MigrationAlgorithm {

	public void performMigration(BC bc);
	
}
